<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Education extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $table    = 'educations';

    public function member(){
        return $this->belongsTo('App\Models\Member');
    }

    public function educationLevel(){
        return $this->belongsTo('App\Models\EducationLevel', 'education_level_id', 'education_level_id');
    }
}
