<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

use File;

class User extends Authenticatable
{
    use HasRoles, SoftDeletes;
    protected $fillable = [
        'name',
        'email',
        'image',
        'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function img_url($size = null){
        $path = 'files/user/'.(!is_null($size) ? $size.'/' : '');
        $url = asset('dashboard/media/users/default.jpg');

        if(File::exists($path.$this->image) && $this->image != ''){
            $url = asset($path.$this->image);
        }
        return $url;
    }
}
