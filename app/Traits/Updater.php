<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait Updater
{
    protected static function boot()
    {
        parent::boot(); /* * During a model create Eloquent will also update the updated_at field so * need to have the updated_by field here as well * */
        static::creating(function ($model) {
            $model->created_by = @Auth::user()->username;
            $model->updated_by = @Auth::user()->username;
        });

        static::updating(function ($model) {
            @$model->updated_by = @Auth::user()->username;
        });
        /*
         * Deleting a model is slightly different than creating or deleting. For
         * deletes we need to save the model first with the deleted_by field
         * */
        static::deleting(function ($model) {
            $model->deleted_by = @Auth::user()->username;
            $model->save();
        });
    }
}
