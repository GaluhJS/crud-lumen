<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EducationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'year_start' => $this->year_start,
            'year_end' => $this->year_end,
            'date' => $this->year_start.' - '.$this->year_end,
            'major' => $this->major,
            'grade' => $this->grade,
            'ipk' => $this->ipk,
            'education_level_id' => $this->education_level_id,
            'member_id' => $this->member_id,
            'education_level' => new EducationLevelResource($this->educationLevel)
        ];
    }
}
