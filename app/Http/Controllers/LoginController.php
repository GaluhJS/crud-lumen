<?php

namespace App\Http\Controllers;

use App\Http\Resources\LoginResource;
use App\Http\Resources\MemberResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Member;
use App\Models\PersonalData;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function __construct(
        Member $model
    ) {
        $this->model = $model;
    }

    public function login(Request $request)
    {
        try {
            $data = $this->model
                ->where('email', $request['email'])
                ->first();
            if ($data) {
                if (Hash::check($request['password'], $data->password)) {
                    
                    $data['token'] = $data->createToken($data->id)->accessToken;

                    $messages = [
                        'code' => 200,
                        'message' => "successfully",
                        'data' => new LoginResource($data)
                    ];
                } else {
                    $messages = [
                        'code' => 400,
                        'message' => "Email dan password Anda salah!"
                    ];
                }
            } else {
                $messages = [
                    'code' => 400,
                    'message' => "Email dan password Anda salah!"
                ];
            }
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }

    public function register(Request $request)
    {
        $input = $request->all();
        try {
            $data = $this->model
                ->where('email', $request['email'])
                ->first();
            if (!$data) {
                if ($input['password'] == $input['password_confirmation']) {
                    $input['password'] = Hash::make($input['password']);
                    $data = $this->model->create($input);
                    $dataPersonal = [
                        'member_id' => $data->id,
                        'phone' => $input['phone_number']
                    ];
                    PersonalData::create($dataPersonal);
                    $data['token'] = $data->createToken($data->id)->accessToken;
    
                    $messages = [
                        'code' => 200,
                        'message' => "successfully",
                        'data' => new LoginResource($data)
                    ];
                }else{
                    $messages = [
                        'code' => 400,
                        'message' => "Password tidak sama!"
                    ];    
                }
            } else {
                $messages = [
                    'code' => 400,
                    'message' => "Email sudah ada!"
                ];
            }
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }
}
