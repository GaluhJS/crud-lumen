<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Http\Resources\NotificationResource;
use App\Models\NotificationRead;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function __construct(
        Notification $model,
        NotificationRead $model_notification_read
    ) {
        $this->model = $model;
        $this->model_notification_read = $model_notification_read;

        $this->member = Auth::guard('member')->user();
        $this->member_id = null;
        if ($this->member)
            $this->member_id = Auth::guard('member')->user()->id;
    }

    public function index()
    {
        try {
            $datas = $this->model
                ->orderBy('created_at', 'DESC')
                ->paginate(10);

            return NotificationResource::collection($datas);
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }

    public function count()
    {
        try {
            $data = $this->model
                ->doesntHave('notification_read')
                ->with('notification_read')
                ->count();

            $messages = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }

    public function update($id)
    {
        try {
            $data = $this->model_notification_read->updateOrCreate(
                ['member_id' => $this->member_id, 'notification_id' => $id],
                ['member_id' => $this->member_id, 'notification_id' => $id]
            );

            $messages = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }

}
