<?php

namespace App\Http\Controllers\Account;

use App\Models\Education;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EducationResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EducationController extends Controller
{
    public function __construct(
        Education $model
    ) {
        $this->model    = $model;
        $this->member_id       = Auth::guard('member')->user()->id;
        // $this->member_id       = '72';
    }

    public function index()
    {
        try {
            $datas = $this->model->whereMemberId($this->member_id)->paginate(10);

            return EducationResource::collection($datas);
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }    

    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $input['member_id'] = $this->member_id;
            $data = $this->model->create($input);

            $messages = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new EducationResource($data)
            ];
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }

    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $data = $this->model->findOrFail($id);
            $data->update($input);

            $messages = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new EducationResource($data)
            ];
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }

    public function destroy($id)
    {
        try {            
            $data = $this->model->findOrFail($id);
            $data->delete();

            $messages = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new EducationResource($data)
            ];
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }
}
